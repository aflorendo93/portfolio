const navone = document.getElementById("navone");
const navtwo = document.getElementById("navtwo");
const navthree = document.getElementById("navthree");
const arrow = document.getElementById("arrow");
const clsBtn = document.getElementById("clsBtn");
const homeBtn = document.getElementById("homeBtn");
const aboutBtn = document.getElementById("aboutBtn");
const capstoneBtn = document.getElementById("capstoneBtn");
const contactBtn = document.getElementById("contactBtn");


//elements
const name = document.getElementById("name");
const types = document.getElementById("type");
const speech = document.getElementById("speech");
const definition = document.getElementById("definition");


// pages
const modal = document.getElementById("modal");
const desc = document.getElementById("desc");
const caps = document.getElementById("caps");
const cont = document.getElementById("cont");

function hide () {
	modal.style.display = "none";
	desc.style.display = "none";
	caps.style.display = "none";
	cont.style.display = "none";
};

function showName(){

	var textWrapper = name;
	textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
	
	anime.timeline({loop: false})
	  .add({
		targets: '#name .letter',
		scale: [4,1],
		opacity: [0,1],
		translateZ: 0,
		easing: "easeOutExpo",
		duration: 10000,
		delay: (el, i) => 1000*i,
		begin: function() {
			name.style.display = 'block';
		}
	  });

};

function showTypes(){
	var textWrapper = types;
	textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
	
	anime.timeline({loop: false})
	  .add({
		targets: '#type .letter',
		scale: [4,1],
		opacity: [0,1],
		translateZ: 0,
		easing: "easeOutExpo",
		duration: 5000,
		delay: (el, i) => 70*i,
		begin: function() {
			types.style.display = 'block';
		}
	  });
};

function showSpeech() {
	var textWrapper = speech;
	textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
	
	anime.timeline({loop: false})
	  .add({
		targets: '#speech .letter',
		scale: [4,1],
		opacity: [0,1],
		translateZ: 0,
		easing: "easeOutExpo",
		duration: 5000,
		delay: (el, i) => 200*i,
		begin: function() {
			speech.style.display = 'block';
		}
	  });

};

function showDefinition() {
	var textWrapper = definition;
	textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
	
	anime.timeline({loop: false})
	  .add({
		targets: '#definition .letter',
		scale: [4,1],
		opacity: [0,1],
		translateZ: 0,
		easing: "easeOutExpo",
		duration: 5000,
		delay: (el, i) => 100*i,
		begin: function() {
			definition.style.display = 'block';
		}
	  });

};

function showArrow(){

	anime.timeline({loop: false})
	  .add({
		targets: '#arrow',
		scale: [4,1],
		opacity: [0,1],
		translateZ: 0,
		easing: "easeOutExpo",
		duration: 5000,
		delay: (el, i) => 70*i,
		begin: function() {
			arrow.style.display = 'block';
		}
	  });


};


function show() {
	setTimeout("showName()", 1000);
	setTimeout("showSpeech()",8000);
	setTimeout("showTypes()",10000);
	setTimeout("showDefinition()",12000);
	setTimeout("showArrow()",18000);
};


window.onload= hide(), show();


arrow.addEventListener("click", function() {
	desc.style.display = "block";
	location.href = "#desc";
});


navone.addEventListener("click", function() {
	modal.style.display = "block";
});

navtwo.addEventListener("click", function() {
	modal.style.display = "block";
});

navthree.addEventListener("click", function() {
	modal.style.display = "block";
});

clsBtn.addEventListener("click", function() {
	modal.style.display = "none";
});

homeBtn.addEventListener("click", function() {
	modal.style.display = "none";
	location.href = "#home";
});

aboutBtn.addEventListener("click", function() {
	modal.style.display = "none";
	desc.style.display = "block";
	caps.style.display = "none";
	cont.style.display = "none";
});

capstoneBtn.addEventListener("click", function() {
	modal.style.display = "none";
	desc.style.display = "none";
	caps.style.display = "block";
	cont.style.display = "none";
});

contactBtn.addEventListener("click", function() {
	modal.style.display = "none";
	desc.style.display = "none";
	caps.style.display = "none";
	cont.style.display = "block";
});





